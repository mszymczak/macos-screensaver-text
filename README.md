# macOS Screensaver Text
This little code exists because I had needed something for running a break-timer for my training.
Runs a screensaver with a clock (current time) and a ten minutes interval.
## Running
Use the original file name:

```
 	./macOS-screensaver-text.sh 15	- runs a screensaver with 15 minutes interval
 	./macOS-screensaver-text.sh		- runs a screensaver with 10 minutes interval (default)
```
or create an alias like `10minutesbreak` and point it to this script (don't forget about `chmod +x`!).

## Testing
This code was tested on:

- macOS High Sierra 10.13.5
- macOS Mojave 10.14 Beta (18A336e)
- macOS Catalina 10.15.1 (19B88)

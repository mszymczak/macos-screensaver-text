#!/bin/bash
# This little code exists because I had need something for running a break-timer for my trainings.
# It runs a screensaver with a clock (current time) and a ten minutes interval.
# Tested on: macOS High Sierra 10.13.5
# Author: Maciej Szymczak <maciej@szymczak.at
# Date: 03.06.2018
#
# Usage:
#	./macOS-screensaver-text.sh <minutes> - runs a screensaver with <minutes> minutes interval
#	./macOS-screensaver-text.sh 15        - runs a screensaver with 15 minutes interval
#	./macOS-screensaver-text.sh           - runs a screensaver with 10 minutes interval (default)

# Check if the system version is correct (ScreenSaver.app in the right place).
SS_app="/System/Library/CoreServices/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine"
if [[ ! -x "${SS_app}" ]]
then
  echo "This is not a macOS High Sierra, you have to manually adjust the ScreenSaverEngine.app path (see source)."
  exit 1
fi

# Check for a proper input (must be a number)
if [[ "${1}" =~ ^[0-9]+$ ]]
then
		# if a value is correct, set the new interval
		interval="${1}"
elif [[ -z "${1}// " ]]
then
		# use default if empty
    interval="10"
else
    # not a number - throw an error
    echo "Input was ignored (this is not a number). Exiting."
    exit 2
fi

startTime="$(date +%H:%M)"                  # current time
endTime="$(date -v"+${interval}M" +%H:%M)"  # +10 minutes
message="Break: ${startTime} - ${endTime}"  # message text here
ssFile="${HOME}/Library/Preferences/ByHost/com.apple.ScreenSaver.Basic.plist"

# set the message for a screensaver
defaults write "${ssFile}" MESSAGE "${message}"

# reload the settings
killall cfprefsd

# start the screensaver
${SS_app}

exit 0
# vim:nowrap expandtab tabstop=2 shiftwidth=2 softtabstop=2 foldmethod=marker background=dark:
